<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 04.08.14
 * Time: 14:05
 */

namespace Report\RouterBundle\Controller;

class RequestBuilder
{
    private $source_request_query;
    private $new_request_query;

    function __construct($request_query="")
    {
        $this->source_request_query= $request_query;
        $this->new_request_query = $this->source_request_query;

        return $this;
    }

    function setHost($hostname)
    {
        $this->new_request_query = "$hostname/api/";

        return $this;
    }

    function selectRouter()
    {
        $this->new_request_query.= "select_router";

        return $this;
    }

    function insertRouter($name, $url)
    {
        $this->new_request_query.= "insert_router?name=$name&url=$url";

        return $this;
    }

    function updateRouter($id, $name, $url)
    {
        $this->new_request_query.= "update_router?id=$id&name=$name&url=$url";

        return $this;
    }

    function deleteRouter($id)
    {
        $this->new_request_query.= "delete_router?id=$id";

        return $this;
    }

    function selectOperator()
    {
        $this->new_request_query.= "select_operator";

        return $this;
    }

    function insertOperator($name, $url, $ip, $host_name, $user, $password, $database, $table_name, $connect_name)
    {
        $this->new_request_query.= "insert_operator?name=$name&url=$url&ip=$ip&host_name=$host_name&user=$user&password=$password&database=$database&table_name=$table_name&connect_name=$connect_name";

        return $this;
    }

    function updateOperator($id, $name, $url, $ip)
    {
        $this->new_request_query.= "update_operator?id=$id&name=$name&url=$url&ip=$ip";

        return $this;
    }

    function deleteOperator($id)
    {
        $this->new_request_query.= "delete_operator?id=$id";

        return $this;
    }

    function selectMethodOfOperator()
    {
        $this->new_request_query.= "select_method_of_operator";

        return $this;
    }

    function insertMethodOfOperator($operator_id, $router_id, $method, $is_demo=0)
    {
        $this->new_request_query.= "insert_method_of_operator?operator_id=$operator_id&router_id=$router_id&method=$method&is_demo=$is_demo";

        return $this;
    }

    function updateMethodOfOperator($id, $operator_id, $router_id, $method, $is_demo=0)
    {
        $this->new_request_query.= "update_method_of_operator?id=$id&operator_id=$operator_id&router_id=$router_id&method=$method&is_demo=$is_demo";

        return $this;
    }

    function deleteMethodOfOperator($id)
    {
        $this->new_request_query.= "delete_method_of_operator?id=$id";

        return $this;
    }

    function updateBaseOfLogs($id, $host_name, $user, $password, $database, $table_name, $connect_name)
    {
        $this->new_request_query.= "update_BaseOfLogs?id=$id&host_name=$host_name&user=$user&password=$password&database=$database&table_name=$table_name&connect_name=$connect_name";

        return $this;
    }

    function putDemoContent($operator, $filename, $content)
    {
        $this->new_request_query.="put_demo_content?operator=$operator&file_name=$filename&content=$content";

        return $this;
    }

    function deleteDemoFile($operator, $filename)
    {
        $this->new_request_query.="delete_demo_file?operator=$operator&file_name=$filename";

        return $this;
    }

    function addCronJob($minutes, $hours, $days, $months, $day_of_the_week, $url, $id)
    {
        $this->new_request_query.= "add_cron_job?minutes=$minutes&hours=$hours&days=$days&months=$months&day_of_the_week=$day_of_the_week&url=$url&id=$id";

        return $this;
    }

    function removeCronJob($url)
    {
        $this->new_request_query.= "remove_cron_job?url=$url";

        return $this;
    }

    function getRequestQuery()
    {
        return $this->new_request_query;
    }
}