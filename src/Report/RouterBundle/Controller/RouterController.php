<?php

namespace Report\RouterBundle\Controller;

use Report\RouterBundle\Controller\RequestBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Report\RouterBundle\Entity\Response;

class RouterController extends Controller
{
    private function getCurrentUser()
    {
        try
        {
            $current_user = $this->getUser();
            if (is_null($current_user)) throw new AccessDeniedException("Пользователь не авторизован!");
            $roles = $current_user->getRoles();
            if (!(in_array('ROLE_ROUTER_USER', $roles))) throw new AccessDeniedException("Нет доступа!");
        }
        catch (Exception $ex)
        {
            return new RedirectResponse($this->generateUrl('login'));
        }
        $user_name = $current_user->getUsername();
        $admin = false;
        if (in_array('ROLE_ROUTER_ADMIN', $roles)) $admin = true;

        return array('username' => $user_name, 'admin' => $admin, 'roles' => $roles);
    }

    private function getForm($input_fields, $action)
    {
        $form = $this->createFormBuilder($data=[]);
        foreach($input_fields as $input)
        {
            $form->add($input, 'text', array('label' => $input));
        }
        $form->add($action, 'submit');
        return $form->getForm();
    }

    private function getResponseObject($id)
    {
        $response_object = $this->getDoctrine()->getRepository('ReportRouterBundle:Response')->find($id);

        return $response_object;
    }

    public function indexAction()
    {
        $user = $this->getCurrentUser();
        $repository = $this->getDoctrine()->getRepository('ReportRouterBundle:Response');
        $responses = $repository->findAll();

        return $this->render('ReportRouterBundle:Router:index.html.twig', array('name' => $user['username'],
            'roles' => $user['roles'], 'responses' => $responses));
    }

    public function newAction(Request $request, $action)
    {
        if ($action == 'select_router') $input_fields = ['title', 'hostname'];
        if ($action == 'insert_router') $input_fields = ['hostname', 'name', 'url'];
        if ($action == 'update_router') $input_fields = ['hostname', 'id', 'name', 'url'];
        if ($action == 'delete_router') $input_fields = ['hostname', 'id'];
        if ($action == 'select_operator') $input_fields = ['title', 'hostname'];
        if ($action == 'insert_operator') $input_fields = ['hostname', 'name', 'url', 'ip', 'host_name', 'user',
                                                           'password','database', 'table_name', 'connect_name'];
        if ($action == 'update_operator') $input_fields = ['hostname', 'id', 'name', 'url', 'ip'];
        if ($action == 'delete_operator') $input_fields = ['hostname', 'id'];
        if ($action == 'select_method_of_operator') $input_fields = ['title', 'hostname'];
        if ($action == 'insert_method_of_operator') $input_fields = ['hostname', 'operator_id', 'router_id', 'method',
                                                                     'is_demo'];
        if ($action == 'update_method_of_operator') $input_fields = ['hostname', 'id','operator_id', 'router_id',
                                                                     'method', 'is_demo'];
        if ($action == 'delete_method_of_operator') $input_fields = ['hostname', 'id'];
        if ($action == 'update_BaseOfLogs') $input_fields = ['hostname', 'id', 'host_name', 'user', 'password',
                                                             'database', 'table_name', 'connect_name'];
        if ($action == 'put_demo_content') $input_fields = ['hostname', 'operator', 'filename', 'content'];
        if ($action == 'delete_demo_file') $input_fields = ['hostname', 'operator', 'filename'];
        if ($action == 'add_cron_job') $input_fields = ['hostname', 'minutes', 'hours', 'days', 'months',
                                                        'day_of_the_week', 'url', 'id'];
        if ($action == 'remove_cron_job') $input_fields = ['hostname', 'url'];
        $form = $this->getForm($input_fields, $action);
        return $this->render('ReportRouterBundle:Router:new.html.twig', array('form' => $form->createView(),
        'action' => $action));
    }

    public function showAction($id)
    {
        if ($response_object = $this->getResponseObject($id))
        {
            $data = unserialize($response_object->getData());
            $columns = array_keys($data[0]);
            $fields = array_reverse(array_values($data));
            return $this->render('ReportRouterBundle:Router:show.html.twig', array('columns' => $columns,
                'fields' => $fields));
        }

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    public function destroyAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $response_object = $this->getResponseObject($id);
        $em->remove($response_object);
        $em->flush();

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    public function requestHandlerAction(Request $request, $action)
    {
        $get_vars = $request->request->get('form');
        $buzz = $this->container->get('buzz');
        if ($action == 'select_router')
        {
            return $this->selectRouter($get_vars, $buzz);
        }
        if ($action == 'insert_router')
        {
            return $this->insertRouter($get_vars, $buzz);
        }
        if ($action == 'update_router')
        {
            return $this->updateRouter($get_vars, $buzz);
        }
        if ($action == 'delete_router')
        {
            return $this->deleteRouter($get_vars, $buzz);
        }
        if ($action == 'select_operator')
        {
            return $this->selectOperator($get_vars, $buzz);
        }
        if ($action == 'insert_operator')
        {
            return $this->insertOperator($get_vars, $buzz);
        }
        if ($action == 'update_operator')
        {
            return $this->updateOperator($get_vars, $buzz);
        }
        if ($action == 'delete_operator')
        {
            return $this->deleteOperator($get_vars, $buzz);
        }
        if ($action == 'select_method_of_operator')
        {
            return $this->selectMethodOfOperator($get_vars, $buzz);
        }
        if ($action == 'insert_method_of_operator')
        {
            return $this->insertMethodOfOperator($get_vars, $buzz);
        }
        if ($action == 'update_method_of_operator')
        {
            return $this->updateMethodOfOperator($get_vars, $buzz);
        }
        if ($action == 'delete_method_of_operator')
        {
            return $this->deleteMethodOfOperator($get_vars, $buzz);
        }
        if ($action == 'update_BaseOfLogs')
        {
            return $this->updateBaseOfLogs($get_vars, $buzz);
        }
        if ($action == 'put_demo_content')
        {
            return $this->putDemoContent($get_vars, $buzz);
        }
        if ($action == 'delete_demo_file')
        {
            return $this->deleteDemoFile($get_vars, $buzz);
        }
        if ($action == 'add_cron_job')
        {
            return $this->addCronJob($get_vars, $buzz);
        }
        if ($action == 'remove_cron_job')
        {
            return $this->removeCronJob($get_vars, $buzz);
        }
    }

    private function selectRouter($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $response_title = $get_vars['title'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->selectRouter()
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        $response_object = new Response();
        $response_object->setData(serialize($data));
        $response_object->setTitle($response_title);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($response_object);
        $em->flush();

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function insertRouter($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $name = $get_vars['name'];
        $url = $get_vars['url'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->insertRouter($name, $url)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function updateRouter($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $id = $get_vars['id'];
        $name = $get_vars['name'];
        $url = $get_vars['url'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->updateRouter($id, $name, $url)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function deleteRouter($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $id = $get_vars['id'];

        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->deleteRouter($id)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function selectOperator($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $response_title = $get_vars['title'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->selectOperator()
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        $response_object = new Response();
        $response_object->setData(serialize($data));
        $response_object->setTitle($response_title);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($response_object);
        $em->flush();

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function insertOperator($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $name = $get_vars['name'];
        $url = $get_vars['url'];
        $ip = $get_vars['ip'];
        $host_name = $get_vars['host_name'];
        $user = $get_vars['user'];
        $password = $get_vars['password'];
        $database = $get_vars['database'];
        $table_name = $get_vars['table_name'];
        $connect_name = $get_vars['connect_name'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->insertOperator($name, $url, $ip, $host_name, $user, $password, $database, $table_name, $connect_name)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function updateOperator($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $id = $get_vars['id'];
        $name = $get_vars['name'];
        $url = $get_vars['url'];
        $ip = $get_vars['ip'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->updateOperator($id, $name, $url, $ip)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function deleteOperator($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $id = $get_vars['id'];

        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
            ->deleteOperator($id)
            ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function selectMethodOfOperator($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $response_title = $get_vars['title'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->selectMethodOfOperator()
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        $response_object = new Response();
        $response_object->setData(serialize($data));
        $response_object->setTitle($response_title);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($response_object);
        $em->flush();

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function insertMethodOfOperator($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $operator_id = $get_vars['operator_id'];
        $router_id = $get_vars['router_id'];
        $method = $get_vars['method'];
        $is_demo = $get_vars['is_demo'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->insertMethodOfOperator($operator_id, $router_id, $method, $is_demo)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function updateMethodOfOperator($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $id = $get_vars['id'];
        $operator_id = $get_vars['operator_id'];
        $router_id = $get_vars['router_id'];
        $method = $get_vars['method'];
        $is_demo = $get_vars['is_demo'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->updateMethodOfOperator($id, $operator_id, $router_id, $method, $is_demo)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function deleteMethodOfOperator($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $id = $get_vars['id'];

        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->deleteMethodOfOperator($id)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function updateBaseOfLogs($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $id = $get_vars['id'];
        $host_name = $get_vars['host_name'];
        $user = $get_vars['user'];
        $password = $get_vars['password'];
        $database = $get_vars['database'];
        $table_name = $get_vars['table_name'];
        $connect_name = $get_vars['connect_name'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->updateBaseOfLogs($id, $host_name, $user, $password, $database, $table_name,
                                                            $connect_name)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function putDemoContent($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $operator = $get_vars['operator'];
        $file_name = $get_vars['file_name'];
        $content = $get_vars['content'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->putDemoContent($operator, $file_name, $content)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function deleteDemoFile($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $operator = $get_vars['operator'];
        $file_name = $get_vars['file_name'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
                                         ->deleteDemoFile($operator, $file_name)
                                         ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function addCronJob($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $minutes = $get_vars['minutes'];
        $hours = $get_vars['hours'];
        $days = $get_vars['days'];
        $months = $get_vars['months'];
        $day_of_the_week = $get_vars['day_of_the_week'];
        $url = $get_vars['url'];
        $id = $get_vars['id'];
        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
            ->addCronJob($minutes, $hours, $days, $months, $day_of_the_week, $url, $id)
            ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

    private function removeCronJob($get_vars, $buzz)
    {
        $hostname = $get_vars['hostname'];
        $url = urlencode($get_vars['url']);

        $request_builder = new RequestBuilder();
        $request_query = $request_builder->setHost($hostname)
            ->removeCronJob($url)
            ->getRequestQuery();
        $response = $buzz->get($request_query)->getContent();
        $data = json_decode($response, true);

        return $this->redirect($this->generateUrl('report_router_homepage'));
    }

}
