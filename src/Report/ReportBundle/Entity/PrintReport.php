<?php

namespace Report\ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="print_report")
 */
class PrintReport
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $report_type;

    /**
     * @ORM\Column(type="integer")
     */
    protected $report_number;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $report_author;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set report_type
     *
     * @param string $report_type
     * @return PrintReport
     */
    public function setReportType($report_type)
    {
        $this->report_type = $report_type;

        return $this;
    }

    /**
     * Get reportType
     *
     * @return string
     */
    public function getReportType()
    {
        return $this->report_type;
    }

    /**
     * Set report_number
     *
     * @param integer $report_number
     * @return PrintReport
     */
    public function setReportNumber($report_number)
    {
        $this->report_number = $report_number;

        return $this;
    }

    /**
     * Get reportNumber
     *
     * @return integer
     */
    public function getReportNumber()
    {
        return $this->report_number;
    }

    /**
     * Set report_author
     *
     * @param string $report_author
     * @return PrintReport
     */
    public function setReportAuthor($report_author)
    {
        $this->report_author = $report_author;

        return $this;
    }

    /**
     * Get reportAuthor
     *
     * @return string
     */
    public function getReportAuthor()
    {
        return $this->report_author;
    }
}