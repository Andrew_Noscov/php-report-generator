<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 13.08.14
 * Time: 13:26
 */
namespace Report\ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Report\ReportBundle\Entity\PrintReport;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Report\ReportBundle\Controller\ReportController as ReportController;
use Symfony\Component\HttpFoundation\Request;

class PrintReportController extends Controller
{
    private function getCurrentUser()
    {
        try
        {
            $current_user = $this->getUser();
            if (is_null($current_user)) throw new AccessDeniedException("Пользователь не авторизован!");
        }
        catch (Exception $ex)
        {
            return new RedirectResponse($this->generateUrl('login'));
        }
        $user_name = $current_user->getUsername();
        $roles = $current_user->getRoles();
        $admin = false;
        if (in_array('ROLE_REPORT_ADMIN', $roles)) $admin = true;

        return array('username' => $user_name, 'admin' => $admin);
    }

    private function getPrintReportObject($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $report = $em->getRepository('ReportBundle:PrintReport')->find($id);
        if (!$report) throw $this->createNotFoundException('Отчет с таким ID не найден '.$id);

        return $report;
    }

    public function printAction()
    {
        $repository = $this->getDoctrine()->getRepository('ReportBundle:PrintReport');
        $print_reports = $repository->findAll();

        return $this->render('ReportBundle:Report:print.html.twig', array('print_reports' => $print_reports));
    }

    public function newPrintAction(Request $request)
    {
        $conn = $this->container->get('doctrine.dbal.db3_connection');
        $report_type = $request->request->get('report_type');
        $error ='';
        if ($report_type == 'agent_report')
        {
            $companies = $this->getCompaniesList("SELECT DISTINCT company_accepter
                                                  FROM bpc_reports_2014.payments_monthly_report;", $conn);
            return $this->render('ReportBundle:Report:new_print_report.html.twig', array('companies' => $companies, 'error' => $error
            ,'report_type' => $report_type));
        }
        if ($report_type == 'activation_cards')
        {
            return $this->render('ReportBundle:Report:new_print_report.html.twig', array('companies' => null, 'error' => $error
            ,'report_type' => $report_type));
        }
        if ($request->request->has('print'))
        {
            $report_type = $request->request->get('type');
            $report = $this->getReport($request, $conn, $report_type);

            return $this->saveReport($report);
        }
    }

    public function deletePrintAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $report = $this->getPrintReportObject($id);

        $em->remove($report);
        $em->flush();

        return $this->redirect($this->generateUrl('report_report_print'));
    }

    private function getReport($request, $conn, $report_type)
    {
        $months_names = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября',
                         'октября', 'ноября', 'декабря'];
        $company = $request->request->get('company');
        $year = $request->request->get('year');
        $months = $request->request->get('months');
        $number = $request->request->get('report_number');
        if ($report_type == 'agent_report')
            return $this->getAgentReport($conn, $months, $company, $year, $report_type, $number, $months_names);
        if ($report_type == 'activation_cards')
            return $this->getActivationCardsReport($conn, $months, $company, $year, $report_type, $number, $months_names);
    }

    private function saveReport($report)
    {
        $current_user = $this->getCurrentUser();
        $print_report = new PrintReport();
        $print_report->SetReportAuthor($current_user['username']);
        $em = $this->getDoctrine()->getEntityManager();
        if ($report['number']) $print_report->SetReportNumber($report['number']);
        else
        {
            $connection = $em->getConnection();
            $query = $connection->prepare('SELECT report_number FROM print_report e ORDER BY e.id DESC LIMIT 1');
            $query->execute();
            $last_number = $query->fetch()['report_number'];
            $print_report->SetReportNumber($last_number+1);
            $report['number'] = $last_number+1;
        }

        $print_report->setReportType($report['type']);

        $em->persist($print_report);
        $em->flush();
        $this->generatePdf($report);

        return $this->redirect($this->generateUrl('report_report_print'));
    }

    private function generatePdf($report)
    {
        $view_name = $report['type'];
        $html = $this->renderView("ReportBundle:Report:$view_name.html.twig",  array('report' => $report));

        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="report.pdf"');
        echo $this->get('knp_snappy.pdf')->getOutputFromHtml($html);
    }

    private function num2str($num)
    {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= $this->morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        $out[] = $kop.' '.$this->morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }

    private function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }

    private function getAgentReport($conn, $months, $company, $year, $report_type, $number, $months_names)
    {
        $months_string = implode(',', $months);
        $report = $conn->fetchAll("SELECT *, SUM(total_summ) as total_summ,
                                   SUM(contract_total_comission) as contract_total_comission
                                   FROM bpc_reports_2014.payments_monthly_report
                                   WHERE company_accepter = '$company' AND payment_year = '$year'
                                   AND payment_month IN ($months_string)");

        $report = $this->prepareReport($months, $year, $report_type, $number, $months_names, $report, $conn);

        return $report;
    }

    private function getActivationCardsReport($conn, $months, $company, $year, $report_type, $number, $months_names)
    {
        $months_string = implode(',', $months);
        $report = $conn->fetchAll("SELECT *,
                                   SUM(cards_activated) as cards_activated
                                   FROM bpc_reports_2014.cards_activation_monthly
                                   WHERE year = '$year'
                                   AND month IN ($months_string)
                                   GROUP BY name");

        $consider = $conn->fetchAll("SELECT company_sender, company_sender_signer
                                   FROM bpc_reports_2014.cards_monthly_report
                                   WHERE payment_year = '$year'
                                   AND payment_month IN ($months_string)
                                   GROUP BY company_sender_signer");
        $companies = $report;

        $report = $this->prepareReport($months, $year, $report_type, $number, $months_names, $report, $conn, $companies,
                                       $consider);

        return $report;
    }

    private function getCompaniesList($query, $conn)
    {
        return $conn->fetchAll($query);
    }

    private function prepareReport($months, $year, $report_type, $number, $months_names, $report, $conn, $companies=null, $consider=null)
    {
        if ($report_type == 'agent_report')
        {
            $report = array_filter($report[0]);
            list($accepter_signer, $sender_signer) = $this->getSigners($conn, $report);
            $report['accepter_name'] = $accepter_signer['name'];
            $report['accepter_familyname'] = $accepter_signer['familyname'];
            $report['accepter_surname'] = $accepter_signer['surname'];
            $report['accepter_position'] = $accepter_signer['position'];
            $report['total_summ_write'] = $this->num2str($report['total_summ']);
            $report['contract_total_comission_write'] = $this->num2str($report['contract_total_comission']);
            $report['result_sum'] = $report['total_summ'] - $report['contract_total_comission'];
            $report['result_sum_write'] = $this->num2str($report['result_sum']);
            $report['total_summ'] = explode(',', number_format($report['total_summ'], 2, ',', ' '));
            $report['result_sum'] = explode(',', number_format($report['result_sum'], 2, ',', ' '));
            $report['contract_total_comission'] = explode(',', number_format($report['contract_total_comission'], 2, ',', ' '));
        }
        if ($report_type == 'activation_cards')
        {
            $report['companies'] = $companies;
            $report['company_sender'] = $consider[0]['company_sender'];
            $consider_id = $consider[0]['company_sender_signer'];
            $sender_signer = $conn->fetchAll("SELECT * FROM bpc_reports_2014.signers WHERE signer_id = '$consider_id'")[0];
        }
        $report['type'] = $report_type;
        $report['number'] = $number;
        $report['months'] = $months;
        $report['months_names'] = $months_names;
        $report['year'] = $year;
        $report['sender_name'] = $sender_signer['name'];
        $report['sender_familyname'] = $sender_signer['familyname'];
        $report['sender_surname'] = $sender_signer['surname'];
        $report['sender_position'] = $sender_signer['position'];
        $report['sender_r_name'] = $sender_signer['r_name'];
        $report['sender_r_familyname'] = $sender_signer['r_familyname'];
        $report['sender_r_surname'] = $sender_signer['r_surname'];
        $report['sender_r_position'] = $sender_signer['r_position'];

        return $report;
    }

    private function getSigners($conn, $report)
    {
        $signer_accepter_id = $report['company_accepter_signer'];
        $signer_sender_id = $report['company_sender_signer'];
        // Почему то тянет несколько одинаковых записей, поэтому просто выбираем первую
        $accepter_signer = $conn->fetchAll("SELECT * FROM bpc_reports_2014.signers WHERE signer_id = '$signer_accepter_id'")[0];
        $sender_signer = $conn->fetchAll("SELECT * FROM bpc_reports_2014.signers WHERE signer_id = '$signer_sender_id'")[0];

        return array($accepter_signer, $sender_signer);
    }
}