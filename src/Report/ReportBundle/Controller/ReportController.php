<?php

namespace Report\ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Report\ReportBundle\Entity\PrintReport;
use Report\ReportBundle\Entity\Report;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Report\ReportBundle\Controller\QueryBuilder as QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

# Как все это работает:
# У сущности Report есть св-во filters по дефолту == Null и columns по дефолту это все колонки из выборки -
# SELECT FROM * view_name;
# Следующий метод используется для получения данных выборки:
# 1) Метод getResult возвращает массив колонок и значений полей с учетом фильтров/скрытых юзером колонок;
# Метод resetQuery устанавливает дефолтное значение sql запроса и колонки его выборки;
# Экшен Filter просто направляет на нужный метод в зависимости от запроса;

class ReportController extends Controller
{
    private function getCurrentUser()
    {
        try
        {
            $current_user = $this->getUser();
            if (is_null($current_user)) throw new AccessDeniedException("Пользователь не авторизован!");
        }
        catch (Exception $ex)
        {
            return new RedirectResponse($this->generateUrl('login'));
        }
        $user_name = $current_user->getUsername();
        $roles = $current_user->getRoles();
        $admin = false;
        if (in_array('ROLE_REPORT_ADMIN', $roles)) $admin = true;

        return array('username' => $user_name, 'admin' => $admin);
    }

    private function getReport($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $report = $em->getRepository('ReportBundle:Report')->find($id);
        if (!$report) throw $this->createNotFoundException('Отчет с таким ID не найден '.$id);

        return $report;
    }

    private function getResult($query, $limit=10, $view_name, $report)
    {
        $conn = $this->container->get('doctrine.dbal.db2_connection');
        $result = $conn->fetchAll($query);
        #$test = $conn->fetchAll("SELECT * FROM bpc_reports_2014.temp_pre_preport_data order by payment_date");
        #var_dump($test);
        if ($result)
        {
            $columns = array_keys($result[0]);
            $builder = new QueryBuilder();
            $queries = [];
            /* Получаем ВСЕ колонки по выборке в массиве вида Default_col_name => Users_col_name
             * Отбрасываем те колонки, что скрыл пользователь, сравнивая массивы по ключам
             * Сохраняем и отдаем пользовательские имена в edit/show
             */
            if ($report->getColumns())
            {
                $columns_names = unserialize($report->getColumns());
                $columns_names = array_intersect_key($columns_names, array_flip($columns));
                $columns_names = array_values($columns_names);
            }
            foreach ($columns as $column)
            {
                array_push($queries, "SELECT DATA_TYPE FROM information_schema.columns WHERE TABLE_NAME = '$view_name'
                AND COLUMN_NAME='$column'");
            }
            $types_query = $builder->makeUnion($queries)
                                   ->getQuery();
            $types = $conn->fetchAll($types_query);
            #Создаем массив вида: Column_name => Column_type
            $columns = array_combine($columns, array_values($types));
            $paginator  = $this->get('knp_paginator');
            $fields = array_values($result);
            $pagination = $paginator->paginate(
                $fields,
                $this->get('request')->query->get('page', 1)/*page number*/,
                $limit/*limit per page*/
            );
            $result = array('columns' => $columns, 'paginate' => $pagination, 'columns_names' => $columns_names,
                            'fields' => $fields);

            return $result;
        }
        else
            return array('columns' => [],'paginate' => []);
    }

    private function getFormCreate($report)
    {
        $views = array('agent_report' => 'agent_report',
                     'agent_report_totals' => 'agent_report_totals',
                     'agent_report_without_payment_types' => 'agent_report_without_payment_types',                    
                     'company_name' => 'company_name',
                     'company_services_by_period' => 'company_services_by_period',
                     'merge_reports' => 'merge_reports');

        $form = $this->createFormBuilder($report)
                     ->add('title', 'text', array('label' => 'Название'))
                     ->add('description', 'textarea', array('label' => 'Описание'))
                     ->add('view','choice', array('label' => 'Представление', 'choices' => $views))
                     ->getForm();

        return $form;        
    }

    private function getFormColumns($columns)
    {
        $data = array();
        $form = $this->createFormBuilder($data);
        $types_list =
            [
                'varchar' => 'text',
                'int' => 'integer'
            ];
        foreach($columns as $column_name => $type)
        {
            $type_name = $type['DATA_TYPE'];
            if (array_key_exists($type_name, $types_list)) $type_name = $types_list[$type_name];
            $form->add($column_name, 'text', array('label' => $column_name, 'attr' => ['placeholder' => 'Изменить название']))
                 ->add("rename_$column_name", 'submit', array('attr' => ['class' => 'btn btn-primary btn-mini',
                       'type' => 'submit']));
        }
        $form = $form->getForm()->createView();
        return $form;
    }

    private function resetQuery($report)
    {
        $query = "SELECT * FROM ".$report->getView();
        $conn = $this->container->get('doctrine.dbal.db2_connection');
        $source_columns = $conn->fetchAll($query);
        $source_columns = array_keys($source_columns[0]);
        $new_columns = [];
        foreach($source_columns as $column_name)
        {
            $new_columns[$column_name] = $column_name;
        }
        $report->setColumns(serialize($new_columns));
        $report->setQuery($query);
    }

    public function indexAction()
    {
        $current_user = $this->getCurrentUser();
        $repository = $this->getDoctrine()->getRepository('ReportBundle:Report');
        $reports = $repository->findAll();

        return $this->render('ReportBundle:Report:index.html.twig', array('user' => $current_user,
            'reports' => $reports));
    }

    public function newAction(Request $request)
    {
        $report = new Report();
        $form = $this->getFormCreate($report);

        if ($request->getMethod() == 'POST') 
        {
            $form->handleRequest($request);

            if ($form->isValid())
            {   
                $current_user = $this->getCurrentUser();
                $report->SetAuthor($current_user['username']);
                $this->resetQuery($report);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($report);
                $em->flush();

                return $this->redirect($this->generateUrl('report_report_homepage'));
            }
        }

        return $this->render('ReportBundle:Report:new.html.twig', array('form' => $form->createView(), ));
    }

    public function destroyAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $report = $this->getReport($id);

        $em->remove($report);
        $em->flush();

        return $this->redirect($this->generateUrl('report_report_homepage'));
    }

    public function showAction(Request $request, $id)
    {
        $report = $this->getReport($id);
        $filters = unserialize($report->getFilters());
        $limit = $filters['limit_per_page'] ? $filters['limit_per_page'] : 10;
        $result = $this->getResult($report->getQuery(), $limit, $report->getView(), $report);

        return $this->render('ReportBundle:Report:show.html.twig', array('report' => $report,
            'paginate' => $result['paginate'], 'visible_columns' => $result['columns'],
            'columns_names' => $result['columns_names'] ));
    }

    public function editAction($id, Request $request)
    {
        $report = $this->getReport($id);

        $form = $this->getFormCreate($report);
        $filters = unserialize($report->GetFilters());
        $limit = $filters['limit_per_page'] ? $filters['limit_per_page'] : 10;
        #Костыль, надо будет подумать над решением
        $old_view_name = $report->getView();
        if ($request->getMethod() == 'POST') 
        {
            $form->handleRequest($request);

            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getEntityManager();
                $em->flush();
                if ($old_view_name != $report->getView())
                {
                    $this->resetQuery($report);
                    $report->setFilters(Null);
                    $em->flush();
                }

                return $this->redirect($this->generateUrl('report_report_edit', array('id' => $id)));
            }
        }
        #Конец костыля
        $result = $this->getResult($report->getQuery(), $limit, $report->getView(), $report);
        $columns_form = $this->getFormColumns($result['columns']);
        $columns = unserialize($report->getColumns());
        $functions = [
            'int' => ['summable', 'max', 'min', 'avg'],
            'varchar' => ['unique'],
            'date' => ['hz'],
            'bigint' => ['summable', 'max', 'min', 'avg'],
            'double' => ['summable', 'max', 'min', 'avg'],
            'decimal' => ['summable', 'max', 'min', 'avg']
        ];

        return $this->render('ReportBundle:Report:edit.html.twig', array('report' => $report, 
            'columns' => $columns, 'paginate' => $result['paginate'], 'visible_columns' => $result['columns'],
            'form' => $form->createView(), 'filters' => $filters, 'limit' => $limit, 'columns_form' => $columns_form,
            'columns_names' => $result['columns_names'], 'functions' => $functions));
    }

    public function filterAction($id, $visible_columns, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $report = $this->getReport($id);
        $view_name = $report->GetView();
        $query_builder = new QueryBuilder();

        $visible_columns = array_keys(json_decode($visible_columns, true));
        if ($request->request->has('filter_delete'))
        {
            return $this->deleteFilters($visible_columns, $query_builder, $view_name, $report, $em);
        }

        if ($request->request->has('apply_filters'))
        {
            return $this->applyFilters($visible_columns, $request, $query_builder, $view_name, $report, $em);
        }
    }

    public function columnsAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $report = $this->getReport($id);
        $view_name = $report->GetView();
        $query_builder = new QueryBuilder();

        $filters = unserialize($report->GetFilters());
        if ($request->request->has('hide_columns'))
        {
            return $this->hideColumns($request, $report, $query_builder, $view_name, $filters, $em);
        }

        if ($request->request->has('rename_columns'))
        {
            return $this->renameColumns($request, $report, $em);
        }

        if ($request->request->has('show_columns'))
        {
            return $this->showColumns($query_builder, $report, $view_name, $filters, $em);
        }

        if ($request->request->has('calculate_columns'))
        {
            return $this->calculateColumns($request, $query_builder, $view_name, $filters, $report, $em);
        }
    }

    private function deleteFilters($visible_columns, $query_builder, $view_name, $report, $em)
    {
        $query = $query_builder->setColumns($visible_columns)
                               ->setTable($view_name)
                               ->getQuery();
        $report->SetQuery($query);
        $report->SetFilters(Null);
        $em->flush();

        return $this->redirect($this->generateUrl('report_report_edit', array('id' => $report->GetId())));
    }

    private function hideColumns(Request $request, $report, $query_builder, $view_name, $filters, $em)
    {
        $hidden_columns = $request->request->get('hidden_columns');
        # В БД колонки хранятся в массиве вида Default_col_name => Users_col_name
        # Нам надо вытащить только оригинальные, поэтому используем array_keys
        $visible_columns = array_diff(array_keys(unserialize($report->getColumns())), $hidden_columns);

        $query = $query_builder->setColumns($visible_columns)
            ->setTable($view_name)
            ->addFilters($filters)
            ->getQuery();

        $report->setQuery($query);
        $em->flush();

        return $this->redirect($this->generateUrl('report_report_edit', array('id' => $report->GetId())));
    }

    private function showColumns($query_builder, $report, $view_name, $filters, $em)
    {
        $query = $query_builder->setColumns(array_keys(unserialize($report->getColumns())))
            ->setTable($view_name)
            ->addFilters($filters)
            ->getQuery();
        $report->SetQuery($query);
        $em->flush();

        return $this->redirect($this->generateUrl('report_report_edit', array('id' => $report->GetId())));
    }

    private function applyFilters($visible_columns, Request $request, $query_builder, $view_name, $report, $em)
    {
        $params = array_filter($request->request->all());
        $query = $query_builder->setColumns($visible_columns)
            ->setTable($view_name)
            ->addFilters($params)
            ->getQuery();
        if ($query) $report->setQuery($query);
        $report->SetFilters(serialize($params));
        $em->flush();

        return $this->redirect($this->generateUrl('report_report_edit', array('id' => $report->GetId())));
    }

    private function renameColumns(Request $request, $report, $em)
    {
        $new_columns_names = array_filter($request->request->get('form'));
        $source_columns = array_keys(unserialize($report->getColumns()));
        /*
         * Получаем массив с исходными именами колонок и массив с пользовательскими именами из формы
         * Оба массива вида Default_col_name => Users_col_name
         * Заполняем новый массив
         * Если имя колонки есть в ключах массива запроса, то значение заносится из формы
         * Если нет, то заносим дефолтное имя.
         * Сохраняем получившийся массив в БД.
         */
        $new_columns = [];
        foreach ($source_columns as $column_name) {
            if (array_key_exists($column_name, $new_columns_names)) {
                $new_columns[$column_name] = $new_columns_names[$column_name];
            } else
                $new_columns[$column_name] = $column_name;
        }
        $report->setColumns(serialize($new_columns));
        $em->flush();

        return $this->redirect($this->generateUrl('report_report_edit', array('id' => $report->GetId())));
    }

    private function calculateColumns(Request $request, $query_builder, $view_name, $filters, $report, $em)
    {
        $columns = $request->request->get('calculated_columns');
        $summable_columns = $request->request->get('summable_columns');
        $group_by_column = $request->request->get('group_by_column');
        $max_columns = $request->request->get('max_columns');
        $min_columns = $request->request->get('min_columns');
        $avg_columns = $request->request->get('avg_columns');

        $query = $query_builder->setColumns($columns)
                               ->addSumFunction($summable_columns)
                               ->addMaxFunction($max_columns)
                               ->addMinFunction($min_columns)
                               ->addAvgFunction($avg_columns)
                               ->setTable($view_name)
                               ->addFilters($filters)
                               ->addGroupBy($group_by_column)
                               ->getQuery();
        $report->setQuery($query);
        $em->flush();

        return $this->redirect($this->generateUrl('report_report_edit', array('id' => $report->GetId())));
    }

}
