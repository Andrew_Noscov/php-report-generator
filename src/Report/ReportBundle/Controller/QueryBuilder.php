<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 29.07.14
 * Time: 17:29
 */
namespace Report\ReportBundle\Controller;

class QueryBuilder
{
    private $source_query;
    private $new_query;

    function __construct($query="")
    {
        $this->source_query = $query;
        $this->new_query = $this->source_query;

        return $this;
    }

    public function setColumns($columns=['*'])
    {
        $list_columns = implode(", ", $columns);
        $this->new_query = "SELECT $list_columns";

        return $this;
    }

    public function setTable($table)
    {
        $this->new_query.= " FROM $table";

        return $this;
    }

    public function addFilters($params)
    {
        if ($params)
        {
            unset($params["limit_per_page"]);
            $columns_names = array_keys($params);
            $filters = array_values($params);
            $this->new_query.=" WHERE $columns_names[0] = '$filters[0]'";
            if (count($filters)>1)
            {
                for($i = 1;$i <= count($filters)-1; $i++)
                {
                    $this->new_query.= " AND "." $columns_names[$i] = '$filters[$i]'";
                }
            }
        }

        return $this;
    }

    public function makeUnion($queries)
    {
        if (!empty($this->new_query)) array_unshift($queries, $this->new_query);
        $this->new_query = implode(' UNION ALL ', $queries);

        return $this;
    }

    public function addSumFunction($columns)
    {
        if ($columns)
        {
            foreach($columns as $column)
                $this->new_query.= ", SUM($column) as $column";
        }

        return $this;
    }

    public function addMaxFunction($columns)
    {
        if ($columns)
        {
            foreach($columns as $column)
                $this->new_query.= ", MAX($column) as $column";
        }

        return $this;
    }

    public function addMinFunction($columns)
    {
        if ($columns)
        {
            foreach($columns as $column)
                $this->new_query.= ", MIN($column) as $column";
        }

        return $this;
    }

    public function addAvgFunction($columns)
    {
        if ($columns)
        {
            foreach($columns as $column)
                $this->new_query.= ", AVG($column) as $column";
        }

        return $this;
    }

    public function addGroupBy($column_name)
    {
        if ($column_name) $this->new_query.= " GROUP BY $column_name";

        return $this;
    }

    public function getQuery()
    {
        return $this->new_query;
    }
}