<?php

namespace Report\UserBundle\Controller;

use Report\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
#use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AdminController extends Controller
{
    private function getCurrentUser()
    {
        try
        {
            $current_user = $this->getUser();
            $roles = $current_user->getRoles();
            if (is_null($current_user)) throw new AccessDeniedException("Пользователь не авторизован!");
            if (!(in_array('ROLE_REPORT_ADMIN', $roles)) or !(in_array('ROLE_ROUTER_ADMIN', $roles)));
        }
        catch (Exception $ex)
        {
            return new RedirectResponse($this->generateUrl('login'));
        }
        $user_name = $current_user->getUsername();
        $admin = false;
        if (in_array('ROLE_REPORT_ADMIN', $roles) or in_array('ROLE_ROUTER_ADMIN', $roles)) $admin = true;

        return array('username' => $user_name, 'admin' => $admin);
    }

    private function getUserObject($id)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));

        return $user;
    }

    private function getFormCreate($user)
    {
        $roles = array(
            'ROLE_REPORT_USER' => 'ROLE_REPORT_USER',
            'ROLE_ROUTER_USER' => 'ROLE_ROUTER_USER',
            'ROLE_REPORT_ADMIN' => 'ROLE_REPORT_ADMIN',
            'ROLE_ROUTER_ADMIN' => 'ROLE_ROUTER_ADMIN'
        );

        $form = $this->createFormBuilder($user)
                     ->add('Username', 'text', array('label' => 'Логин'))
                     ->add('Email', 'email', array('label' => 'E-mail'))
                     ->add('Password', 'password', array('label' => 'Пароль'))
                     ->add('Roles', 'choice', array('label' => 'Роли', 'choices' => $roles, 'multiple' => true))
                     ->add('Применить', 'submit', array('attr' => array('class' => 'btn btn-success')))
                     ->getForm();
        return $form;
    }

    public function indexAction()
    {
        $this->getCurrentUser();
        $userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();

        return $this->render('UserBundle:Admin:index.html.twig', array('users' => $users));
    }

    public function deleteUserAction($id)
    {
        $user = $this->getUserObject($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirect($this->generateUrl('report_admin_homepage'));
    }

    public function createUserAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $form = $this->getFormCreate($user);
        if ($request->getMethod() == 'POST')
        {
            $form_data = $request->request->get('form');
            $user->setEnabled(true);
            $user->setUsername($form_data['Username']);
            $user->setEmail($form_data['Email']);
            $user->setPlainPassword($form_data['Password']);
            $user->setRoles(array_values($form_data['Roles']));
            $userManager->updateUser($user, true);

            return $this->redirect($this->generateUrl('report_admin_homepage'));
        }

        return $this->render('UserBundle:Admin:new.html.twig', array('form' => $form->createView()));
    }

    public function editUserAction(Request $request, $id)
    {
        $user = $this->getUserObject($id);
        $form = $this->getFormCreate($user);
        if ($request->getMethod() == 'POST')
        {
            $form_data = $request->request->get('form');
            $user->setEnabled(true);
            $user->setUsername($form_data['Username']);
            $user->setEmail($form_data['Email']);
            $user->setPlainPassword($form_data['Password']);
            $user->setRoles(array_values($form_data['Roles']));
            $this->get('fos_user.user_manager')->updateUser($user, true);

            return $this->redirect($this->generateUrl('report_admin_homepage'));
        }

        return $this->render('UserBundle:Admin:edit.html.twig', array('form' => $form->createView(), 'id' => $id));
    }
}
