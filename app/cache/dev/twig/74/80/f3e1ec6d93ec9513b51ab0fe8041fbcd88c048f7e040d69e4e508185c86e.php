<?php

/* ReportBundle:Report:edit.html.twig */
class __TwigTemplate_7480f3e1ec6d93ec9513b51ab0fe8041fbcd88c048f7e040d69e4e508185c86e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $_trait_0 = $this->env->loadTemplate("form_table_layout.html.twig");
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_table_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'body' => array($this, 'block_body'),
                'field_row' => array($this, 'block_field_row'),
            )
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => $this));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "<div class=\"container\">
    <h1>Редактирование отчета</h1>
    <div class=\"row\">
    <div class=\"span12\">
    <form action=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("report_report_edit", array("id" => $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " >
        ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
         <p>
            <button type=\"submit\" class=\"btn btn-success\">Подтвердить изменения</button>
            <a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("report_report_homepage");
        echo "\" class=\"btn btn-primary\">Назад к списку</a>
        </p>
    </form>

        ";
        // line 25
        echo "        <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("report_report_columns", array("id" => $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "id"))), "html", null, true);
        // line 26
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["columns_form"]) ? $context["columns_form"] : $this->getContext($context, "columns_form")), 'enctype');
        echo " >
        <p><b>Колонки: </b></p>
        <hr>
        <div class=\"row\">
            <div class=\"span3\">
                <p>
                    <button type=\"submit\" class=\"btn btn-mini btn-danger\" value=\"hide_columns\" name=\"hide_columns\"
                            formnovalidate=\"formnovalidate\">
                        Скрыть колонки
                    </button>
                    <button type=\"submit\" class=\"btn btn-mini btn-primary\" value=\"show_columns\" name=\"show_columns\"
                            formnovalidate=\"formnovalidate\">
                        Показать все
                    </button>
                </p>
                <select name=\"hidden_columns[]\" multiple=\"multiple\">
                    ";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["columns"]) ? $context["columns"] : $this->getContext($context, "columns")));
        foreach ($context['_seq'] as $context["column"] => $context["column_name"]) {
            if ((isset($context["columns"]) ? $context["columns"] : $this->getContext($context, "columns"))) {
                // line 43
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, (isset($context["column"]) ? $context["column"] : $this->getContext($context, "column")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, (isset($context["column"]) ? $context["column"] : $this->getContext($context, "column")), "html", null, true);
                echo ")</option>
                    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['column'], $context['column_name'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                </select>
            </div>
            <div class=\"span11\">
                ";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        foreach ($context['_seq'] as $context["column_name"] => $context["type"]) {
            if ((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns"))) {
                // line 49
                echo "                    <p>
                        ";
                // line 50
                $this->displayBlock('field_row', $context, $blocks);
                // line 54
                echo "                        <input type=\"checkbox\" name=\"calculated_columns[]\" value=\"";
                echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                echo "\">Вычислять таблицу
                        <input type=\"checkbox\" name=\"group_by_column\" value=\"";
                // line 55
                echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                echo "\">Группировать по ней<br>
                        ";
                // line 56
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["functions"]) ? $context["functions"] : $this->getContext($context, "functions")), $this->getAttribute((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "DATA_TYPE"), array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["function"]) {
                    // line 57
                    echo "                            ";
                    echo twig_escape_filter($this->env, (isset($context["function"]) ? $context["function"] : $this->getContext($context, "function")), "html", null, true);
                    echo ": <input type=\"checkbox\" name=\"";
                    echo twig_escape_filter($this->env, (isset($context["function"]) ? $context["function"] : $this->getContext($context, "function")), "html", null, true);
                    echo "_columns[]\" value=\"";
                    echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                    echo "\">
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['function'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo "                    </p>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['column_name'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "                <button type=\"submit\" class=\"btn btn-mini btn-primary\" value=\"rename_columns\" name='rename_columns'
                        formnovalidate=\"formnovalidate\">
                    Переименовать
                </button>
                <button type=\"submit\" class=\"btn btn-mini btn-primary\" value=\"calculate_columns\" name='calculate_columns'
                        formnovalidate=\"formnovalidate\">
                    Вычислить
                </button>
            </div>
            <div class=\"span9\" style=\"margin-top: 10px\">
                <p>
                    Сортировать по колонке:
                    <select>
                        ";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns")));
        foreach ($context['_seq'] as $context["column_name"] => $context["type"]) {
            if ((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns"))) {
                // line 75
                echo "                            <option value=\"";
                echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                echo "</option>
                        ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['column_name'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                    </select>
                    <select>
                        <option value=\"ASC\">По возрастанию</option>
                        <option value=\"DESC\">По убыванию</option>
                    </select>
                </p>
            </div>
        </div>
        </form>
        <form action=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("report_report_filter", array("id" => $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "id"), "visible_columns" => twig_jsonencode_filter((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns"))))), "html", null, true);
        echo "\" method=\"post\"
                ";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " >
        <p><b>Фильтры: </b></p>
        <hr>
        <div class=\"row\">
            <div class=\"span3\">
                <p>Кол-во записей на странице: <b>";
        // line 92
        echo twig_escape_filter($this->env, (isset($context["limit"]) ? $context["limit"] : $this->getContext($context, "limit")), "html", null, true);
        echo "</b> </p>
                <select name=\"limit_per_page\">
                    <option value=\"10\">10</option>
                    <option value=\"25\">25</option>
                    <option value=\"50\">50</option>
                </select>
            </div>
        ";
        // line 99
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns")));
        foreach ($context['_seq'] as $context["column_name"] => $context["type"]) {
            if ((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns"))) {
                // line 100
                echo "            <div class=\"span3\">
            <p>";
                // line 101
                echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                echo ": </p>
            <input class=\"input-medium\" type=\"text\" name=\"";
                // line 102
                echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                echo "\"
                ";
                // line 103
                if (twig_in_filter((isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), twig_get_array_keys_filter((isset($context["filters"]) ? $context["filters"] : $this->getContext($context, "filters"))))) {
                    // line 104
                    echo "                value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["filters"]) ? $context["filters"] : $this->getContext($context, "filters")), (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), array(), "array"), "html", null, true);
                    echo "\"
                ";
                }
                // line 106
                echo "            >
            </div>
        ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['column_name'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 109
        echo "        </div>
        <br>
        <p>
            <button type=\"submit\" class=\"btn btn-success\" name=\"apply_filters\">Применить фильтр</button>
            <button type=\"submit\" class=\"btn btn-danger\" value=\"Delete\" name=\"filter_delete\">Сбросить</button>
        </p>
    </form>
    </div>
    </div>

    <table class=\"table table-bordered\">
        <tr>
            ";
        // line 121
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        foreach ($context['_seq'] as $context["column_name"] => $context["type"]) {
            if ((isset($context["visible_columns"]) ? $context["visible_columns"] : $this->getContext($context, "visible_columns"))) {
                // line 122
                echo "                <th>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["columns_names"]) ? $context["columns_names"] : $this->getContext($context, "columns_names")), $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index0"), array(), "array"), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name")), "html", null, true);
                echo ")|(";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "DATA_TYPE"), "html", null, true);
                echo ")</th>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['column_name'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "        </tr>
        ";
        // line 125
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["paginate"]) ? $context["paginate"] : $this->getContext($context, "paginate")));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            if ((isset($context["paginate"]) ? $context["paginate"] : $this->getContext($context, "paginate"))) {
                // line 126
                echo "            <tr>
                ";
                // line 127
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")));
                foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
                    // line 128
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, (isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "html", null, true);
                    echo "</td>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 130
                echo "            </tr>
        ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "    </table>
    <div id=\"navigation\">
        ";
        // line 134
        if ((isset($context["paginate"]) ? $context["paginate"] : $this->getContext($context, "paginate"))) {
            // line 135
            echo "            ";
            echo $this->env->getExtension('knp_pagination')->render((isset($context["paginate"]) ? $context["paginate"] : $this->getContext($context, "paginate")));
            echo "
        ";
        }
        // line 137
        echo "    </div>
</div>

";
    }

    // line 50
    public function block_field_row($context, array $blocks = array())
    {
        // line 51
        echo "                        ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["columns_form"]) ? $context["columns_form"] : $this->getContext($context, "columns_form")), (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name"))), 'label');
        echo "
                        ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["columns_form"]) ? $context["columns_form"] : $this->getContext($context, "columns_form")), (isset($context["column_name"]) ? $context["column_name"] : $this->getContext($context, "column_name"))), 'widget', array("value" => $this->getAttribute((isset($context["columns_names"]) ? $context["columns_names"] : $this->getContext($context, "columns_names")), $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index0"), array(), "array")));
        echo "
                        ";
    }

    public function getTemplateName()
    {
        return "ReportBundle:Report:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 132,  335 => 130,  319 => 126,  311 => 124,  248 => 102,  244 => 101,  236 => 99,  226 => 92,  218 => 87,  160 => 59,  14 => 1,  110 => 13,  1082 => 340,  1066 => 334,  1062 => 333,  1053 => 332,  1050 => 331,  1038 => 326,  1032 => 324,  1026 => 322,  1022 => 320,  1018 => 319,  997 => 312,  991 => 310,  985 => 308,  981 => 306,  977 => 305,  973 => 304,  969 => 303,  965 => 302,  959 => 301,  948 => 296,  941 => 294,  932 => 287,  930 => 286,  926 => 285,  918 => 280,  910 => 278,  906 => 277,  904 => 276,  893 => 271,  890 => 270,  886 => 267,  883 => 265,  881 => 264,  869 => 258,  845 => 257,  842 => 255,  839 => 253,  837 => 252,  835 => 251,  828 => 247,  824 => 245,  821 => 244,  817 => 239,  814 => 238,  808 => 234,  806 => 233,  803 => 232,  799 => 229,  795 => 227,  793 => 226,  791 => 225,  788 => 224,  784 => 221,  781 => 216,  776 => 212,  753 => 206,  747 => 203,  744 => 202,  741 => 200,  739 => 199,  737 => 198,  734 => 197,  728 => 191,  725 => 190,  719 => 186,  716 => 185,  703 => 180,  701 => 179,  698 => 178,  694 => 175,  692 => 174,  689 => 173,  685 => 170,  675 => 165,  673 => 164,  670 => 163,  665 => 160,  663 => 159,  660 => 158,  654 => 154,  651 => 153,  647 => 150,  638 => 145,  635 => 144,  631 => 141,  629 => 140,  626 => 139,  622 => 136,  619 => 135,  601 => 128,  596 => 127,  591 => 124,  589 => 123,  581 => 118,  579 => 116,  578 => 115,  577 => 114,  576 => 113,  572 => 112,  569 => 110,  567 => 109,  564 => 108,  558 => 104,  552 => 101,  550 => 100,  546 => 99,  541 => 96,  524 => 92,  521 => 91,  507 => 88,  504 => 87,  479 => 82,  476 => 80,  467 => 77,  465 => 76,  445 => 74,  441 => 71,  439 => 70,  431 => 66,  425 => 63,  414 => 60,  412 => 59,  404 => 58,  401 => 57,  399 => 56,  397 => 55,  394 => 54,  389 => 51,  373 => 46,  370 => 52,  357 => 37,  349 => 135,  346 => 33,  342 => 30,  330 => 23,  326 => 128,  323 => 19,  321 => 18,  317 => 17,  300 => 13,  295 => 11,  290 => 7,  282 => 3,  275 => 330,  270 => 316,  265 => 299,  263 => 294,  260 => 106,  257 => 291,  255 => 284,  250 => 274,  245 => 270,  242 => 269,  237 => 262,  232 => 249,  212 => 224,  194 => 197,  191 => 75,  181 => 185,  178 => 184,  146 => 147,  134 => 54,  129 => 49,  126 => 121,  124 => 108,  114 => 91,  81 => 32,  76 => 25,  1405 => 493,  1401 => 491,  1398 => 490,  1396 => 489,  1394 => 488,  1391 => 487,  1381 => 481,  1378 => 480,  1361 => 477,  1358 => 476,  1355 => 475,  1352 => 474,  1346 => 472,  1343 => 471,  1340 => 470,  1334 => 468,  1328 => 466,  1325 => 465,  1322 => 463,  1306 => 461,  1303 => 460,  1300 => 459,  1297 => 458,  1294 => 457,  1291 => 456,  1288 => 455,  1285 => 454,  1282 => 453,  1279 => 452,  1276 => 451,  1273 => 450,  1270 => 449,  1268 => 448,  1266 => 447,  1263 => 446,  1256 => 439,  1250 => 437,  1244 => 435,  1242 => 434,  1240 => 433,  1237 => 432,  1228 => 422,  1221 => 420,  1218 => 416,  1214 => 415,  1208 => 413,  1205 => 412,  1190 => 410,  1187 => 409,  1183 => 408,  1180 => 407,  1162 => 406,  1154 => 403,  1151 => 396,  1146 => 395,  1143 => 394,  1141 => 393,  1139 => 392,  1137 => 391,  1134 => 390,  1127 => 385,  1118 => 383,  1114 => 382,  1111 => 381,  1108 => 380,  1106 => 379,  1103 => 378,  1093 => 372,  1091 => 371,  1089 => 369,  1086 => 368,  1076 => 338,  1070 => 336,  1068 => 335,  1065 => 360,  1057 => 355,  1046 => 353,  1044 => 352,  1024 => 321,  1021 => 350,  1012 => 318,  1009 => 317,  1006 => 343,  1003 => 342,  1001 => 341,  998 => 340,  989 => 335,  986 => 334,  983 => 307,  980 => 332,  978 => 331,  975 => 330,  966 => 326,  958 => 324,  956 => 300,  954 => 322,  951 => 321,  944 => 295,  939 => 314,  937 => 313,  934 => 312,  927 => 306,  923 => 284,  917 => 303,  915 => 302,  907 => 301,  902 => 275,  899 => 274,  897 => 298,  894 => 297,  885 => 293,  882 => 292,  878 => 263,  876 => 289,  871 => 259,  863 => 286,  860 => 285,  857 => 284,  852 => 283,  850 => 282,  847 => 281,  832 => 250,  829 => 273,  826 => 246,  813 => 268,  810 => 235,  807 => 266,  800 => 263,  797 => 228,  789 => 256,  783 => 254,  780 => 253,  774 => 251,  771 => 250,  765 => 248,  762 => 247,  756 => 208,  754 => 244,  750 => 205,  746 => 242,  730 => 192,  727 => 240,  724 => 239,  721 => 187,  718 => 237,  715 => 236,  712 => 235,  709 => 234,  706 => 182,  704 => 232,  702 => 231,  699 => 230,  690 => 225,  688 => 224,  683 => 169,  680 => 168,  677 => 221,  674 => 220,  672 => 219,  669 => 218,  661 => 212,  658 => 211,  656 => 155,  653 => 209,  645 => 149,  642 => 148,  640 => 203,  637 => 202,  628 => 196,  624 => 195,  620 => 194,  616 => 133,  611 => 129,  608 => 191,  602 => 189,  599 => 188,  597 => 187,  594 => 126,  586 => 122,  583 => 180,  573 => 179,  568 => 178,  565 => 177,  559 => 175,  556 => 103,  554 => 102,  551 => 172,  543 => 97,  540 => 166,  538 => 95,  537 => 164,  536 => 163,  535 => 162,  532 => 161,  529 => 160,  526 => 159,  523 => 158,  516 => 155,  506 => 148,  500 => 145,  496 => 144,  492 => 142,  486 => 140,  484 => 139,  475 => 138,  472 => 78,  466 => 135,  464 => 134,  450 => 132,  448 => 75,  446 => 130,  443 => 129,  432 => 122,  429 => 65,  426 => 120,  420 => 118,  418 => 117,  410 => 115,  408 => 114,  385 => 111,  383 => 49,  377 => 47,  375 => 106,  372 => 105,  367 => 102,  361 => 100,  359 => 99,  353 => 97,  350 => 96,  347 => 134,  339 => 28,  334 => 26,  331 => 90,  328 => 22,  318 => 87,  316 => 86,  308 => 85,  302 => 83,  299 => 82,  296 => 81,  293 => 80,  287 => 5,  274 => 72,  249 => 69,  222 => 238,  216 => 54,  211 => 53,  205 => 51,  200 => 50,  197 => 49,  192 => 48,  184 => 46,  172 => 39,  161 => 162,  77 => 23,  58 => 12,  225 => 59,  210 => 43,  207 => 216,  198 => 39,  188 => 194,  186 => 74,  180 => 34,  148 => 34,  118 => 48,  65 => 9,  113 => 45,  104 => 74,  100 => 15,  97 => 38,  84 => 33,  34 => 14,  53 => 10,  23 => 3,  480 => 162,  474 => 79,  469 => 158,  461 => 155,  457 => 153,  453 => 133,  444 => 149,  440 => 148,  437 => 69,  435 => 123,  430 => 144,  427 => 64,  423 => 62,  413 => 116,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 113,  387 => 122,  384 => 121,  381 => 48,  379 => 108,  374 => 116,  368 => 112,  365 => 51,  362 => 50,  360 => 38,  355 => 137,  341 => 93,  337 => 27,  322 => 127,  314 => 125,  312 => 98,  309 => 97,  305 => 95,  298 => 12,  294 => 122,  285 => 4,  283 => 121,  278 => 331,  268 => 300,  264 => 84,  258 => 71,  252 => 103,  247 => 273,  241 => 100,  229 => 73,  220 => 70,  214 => 86,  177 => 65,  169 => 168,  140 => 55,  132 => 50,  128 => 27,  107 => 16,  61 => 4,  273 => 317,  269 => 109,  254 => 104,  243 => 67,  240 => 263,  238 => 65,  235 => 250,  230 => 244,  227 => 243,  224 => 241,  221 => 77,  219 => 237,  217 => 232,  208 => 68,  204 => 215,  179 => 44,  159 => 158,  143 => 56,  135 => 53,  119 => 95,  102 => 32,  71 => 25,  67 => 7,  63 => 22,  59 => 21,  38 => 2,  94 => 42,  89 => 41,  85 => 14,  75 => 32,  68 => 30,  56 => 2,  87 => 25,  21 => 2,  26 => 4,  93 => 11,  88 => 15,  78 => 21,  46 => 7,  27 => 1,  44 => 7,  31 => 4,  28 => 2,  201 => 213,  196 => 211,  183 => 189,  171 => 61,  166 => 167,  163 => 36,  158 => 67,  156 => 157,  151 => 152,  142 => 32,  138 => 54,  136 => 138,  121 => 107,  117 => 24,  105 => 12,  91 => 44,  62 => 7,  49 => 8,  24 => 3,  25 => 3,  19 => 1,  79 => 26,  72 => 31,  69 => 11,  47 => 7,  40 => 8,  37 => 10,  22 => 1,  246 => 68,  157 => 56,  145 => 33,  139 => 55,  131 => 132,  123 => 26,  120 => 25,  115 => 23,  111 => 90,  108 => 36,  101 => 73,  98 => 31,  96 => 53,  83 => 32,  74 => 26,  66 => 7,  55 => 11,  52 => 11,  50 => 3,  43 => 6,  41 => 4,  35 => 6,  32 => 5,  29 => 4,  209 => 223,  203 => 77,  199 => 212,  193 => 38,  189 => 71,  187 => 47,  182 => 45,  176 => 178,  173 => 177,  168 => 72,  164 => 163,  162 => 57,  154 => 153,  149 => 148,  147 => 57,  144 => 144,  141 => 143,  133 => 23,  130 => 28,  125 => 44,  122 => 43,  116 => 94,  112 => 22,  109 => 87,  106 => 86,  103 => 32,  99 => 43,  95 => 16,  92 => 21,  86 => 34,  82 => 13,  80 => 25,  73 => 7,  64 => 15,  60 => 22,  57 => 5,  54 => 19,  51 => 18,  48 => 11,  45 => 17,  42 => 20,  39 => 19,  36 => 13,  33 => 12,  30 => 3,);
    }
}
