<?php

/* ReportBundle:Report:index.html.twig */
class __TwigTemplate_78430fad5b52b0deb9118b9599355227de5358bf7c41de516840ebf737246795 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
    <div class=\"navbar\">
        <div class=\"navbar-inner\">
            <ul class=\"nav\">
                <li><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("report_admin_homepage");
        echo "\">Панель управления</a></li>
                <li class=\"active\"><a href=\"#\">Отчеты</a></li>
                <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("report_router_homepage");
        echo "\">Логи</a></li>
            </ul>
            <ul class=\"nav pull-right\">
                <li><a href=\"logout\">Выйти</a></li>
            </ul>
        </div>
    </div>
    <h2> Здравствуйте, ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array(), "array"), "html", null, true);
        echo "! </h2>
    <a href=\"../profile\">Профиль</a>
    |
    <a href=\"../logout\">Выйти</a>
    ";
        // line 21
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "admin", array(), "array")) {
            // line 22
            echo "        <p>У вас привилегии администратора</p>
    ";
        }
        // line 24
        echo "    <h3>Cписок отчетов: </h3>
    <p>
    <a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("report_report_new");
        echo "\" class=\"btn btn-primary\">Добавить отчет</a>
    <a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("report_report_print");
        echo "\" class=\"btn btn-info\">История печати</a>
    </p>
    <hr>
    <table class=\"table table-bordered\">
        <tr>
            <th>Название</th>
            <th>Описание</th>
            <th>Автор</th>
            <th>Представление</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        ";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reports"]) ? $context["reports"] : $this->getContext($context, "reports")));
        foreach ($context['_seq'] as $context["_key"] => $context["report"]) {
            // line 41
            echo "        <tr>
            <td>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "title"), "html", null, true);
            echo "</td>
            <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "description"), "html", null, true);
            echo "</td>
            <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "author"), "html", null, true);
            echo "</td>
            <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "view"), "html", null, true);
            echo "</td>
            <td><a href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("report_report_show", array("id" => $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "id"))), "html", null, true);
            echo "\" class=\"btn btn-info\">Просмотр</a></td>
            <td><a href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("report_report_edit", array("id" => $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary\">Редактировать</a></td>
            <td><a href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("report_report_destroy", array("id" => $this->getAttribute((isset($context["report"]) ? $context["report"] : $this->getContext($context, "report")), "id"))), "html", null, true);
            echo "\" class=\"btn btn-danger\">Удалить</a></td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['report'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "ReportBundle:Report:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 51,  120 => 48,  116 => 47,  112 => 46,  108 => 45,  104 => 44,  100 => 43,  96 => 42,  93 => 41,  89 => 40,  73 => 27,  69 => 26,  65 => 24,  61 => 22,  59 => 21,  52 => 17,  42 => 10,  37 => 8,  31 => 4,  28 => 3,);
    }
}
