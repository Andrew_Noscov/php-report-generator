<?php

/* UserBundle:Admin:index.html.twig */
class __TwigTemplate_aae1538e6c72559c185b67f6e56d7705ddeeecba8547f9df696b1c56eb825a72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"container\">
        <div class=\"navbar\">
            <div class=\"navbar-inner\">
                <ul class=\"nav\">
                    <li class=\"active\"><a href=\"#\">Панель управления</a></li>
                    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("report_report_homepage");
        echo "\">Отчеты</a></li>
                    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("report_router_homepage");
        echo "\">Логи</a></li>
                </ul>
                <ul class=\"nav pull-right\">
                    <li><a href=\"logout\">Выйти</a></li>
                </ul>
            </div>
        </div>
        <h3>Список пользователей</h3>
        <p>
            <a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("report_admin_createUser");
        echo "\" class=\"btn btn-primary\">Добавить пользователя</a>
        </p>
        <hr>
        <table class=\"table table-bordered\">
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Роли</th>
                <th></th>
                <th></th>
            </tr>
            ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 31
            echo "                <tr>
                    <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "id"), "html", null, true);
            echo "</td>
                    <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username"), "html", null, true);
            echo "</td>
                    <td>
                        ";
            // line 35
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "roles"));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                // line 36
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")), "html", null, true);
                echo ";
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "                    </td>
                    <td><a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("report_admin_editUser", array("id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary\">Редактировать</a></td>
                    <td><a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("report_admin_deleteUser", array("id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "id"))), "html", null, true);
            echo "\" class=\"btn btn-danger\">Удалить</a></td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "        </table>
    </div>
";
    }

    public function getTemplateName()
    {
        return "UserBundle:Admin:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 43,  104 => 40,  100 => 39,  97 => 38,  88 => 36,  84 => 35,  79 => 33,  75 => 32,  72 => 31,  68 => 30,  54 => 19,  42 => 10,  38 => 9,  31 => 4,  28 => 3,);
    }
}
